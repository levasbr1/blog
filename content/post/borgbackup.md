---
title: "Borg Backup" # Title of the blog post.
date: 2020-09-09T14:12:36+02:00 # Date of post creation.
description: "Sauvegarde avec BorgBackup" # Description used for search engine.
featured: true # Sets if post is a featured post, making appear on the home page side bar.
draft: false # Sets whether to render this page. Draft of true will not be rendered.
toc: false # Controls if a table of contents should be generated for first-level links automatically.
# menu: main
featureImage: "/images/borg.jpg" # Sets featured image on blog post.
thumbnail: "/images/borg.jpg" # Sets thumbnail image appearing inside card on homepage.
shareImage: "/images/path/share.png" # Designate a separate image for social media sharing.
codeMaxLines: 20 # Override global value for how many lines within a code block before auto-collapsing.
codeLineNumbers: false # Override global value for showing of line numbers within code block.
figurePositionShow: true # Override global value for showing the figure label.
categories:
  - SysAdmin
tags:
  - backup

---

BorgBackup est une solution de sauvegarde élégante qui supporte à la fois la déduplication, la compression et le chiffrement...

<!--more-->

BorgBackup est un outil en CLI écrit en python simple d'utilisation. Il fonctionne de manière incrémentale en découpant chaque fichier en morceau ( chunck ). Entre deux sauvegardes, seuls les nouveaux chunks sont copiés sur le dépot ( repository ) en préservant ainsi l'espace disque et la bande passante. Borg conserve un cache coté client ( par défaut: ~/.cache/borg/ ) qui contient les checksums de tous les chunks déjà sauvegardés.

Les fonctionnalités:

- compression: LZ4, ZSTD, ZLIB et LZMA
- chiffrement coté client: AES256
- SSH pour les sauvegardes distantes
- multi plateformes: Linux, macOS, BSD
- contrôles d'intégrités (checksum)
- dépot accessible via FUSE

# Installation

BorgBackup est disponible sur la [plupart des distributions](https://borgbackup.readthedocs.io/en/stable/installation.html) ou directement sous forme d'un [binaire](https://github.com/borgbackup/borg/releases)

# Mise en oeuvre

Dans cet exemple, on part du principe que le client ( client.domain.tld ) peut accéder en SSH au serveur de backup ( backup.domain.tld ). Sur celui-ci, on utilise un `user` et un `reposotory` dédié pour chaque serveur client. Ce n'est pas forcément optimal pour la déduplication entre serveurs (à étudier si vous sauvegardez des images disques de VMs par exemple) mais ça évite de mettre tous ses oeufs dans le même panier en cas de corruption du repository et ça rend l'exploitation plus simple au quotidien.

- sur le serveur de backup, on crée un utilisateur dédié pour chaque client ( borg-<hostname_du_client ). On en profite pour créer un répertoire idoine dans `/srv` (prévoir un système de fichier dédié si possible)

```
[root@backup ~]# useradd borg-client
[root@backup ~]# passwd borg-client
[root@backup ~]# mkdir /srv/borg-client
[root@backup ~]# chown borg-client:borg-client /srv/borg-client
[root@backup ~]# chmod 750 /srv/borg-client
```

- sur le client, on configure un accès SSH authentifié par clé public

```
[root@client ~]# ssh-keygen -t rsa -b 4096 -C "borg-client@client.domain.tld"
[root@client ~]# ssh-copy-id borg-client@backup.domain.tld
```
> la cible des commandes ( client ou backup ) est identifiée par le prompt...

- sur le serveur de sauvegarde, on modifie le fichier `authorized_keys` pour limiter les actions possibles en SSH et éviter ainsi la suppression des backups en cas de compromission du client

  - [--restrict-to-path](https://borgbackup.readthedocs.io/en/stable/usage/serve.html)
  - [--append-only](https://borgbackup.readthedocs.io/en/stable/usage/notes.html#append-only-mode)

```
[root@backup ~]# cat /home/borg-client/.ssh/authorized_keys
  command="borg serve --append-only --restrict-to-path /srv/client",restrict ssh-rsa <votre-clé-publique>
```

# Initialisation du repository Borg

Dans cette exemple, on n'utilise pas le chiffrement des données sur le serveur de backup situé en interne ( à conseiller avec un prestataire externe )

```
[root@client ~]# borg init --encryption none borg-client@backup.domain.tld:/srv/client
```

on lance une première sauvegarde (dans un TMUX si la volumétrie est conséquente):

```
[root@client ~]# borg create -s --progress borg-client@backup.domain.tld:/srv/client::{now:%Y-%m-%d} /path/to/save
```

et enfin, on modifie la crontab pour automatiser l'opération:

```
05 01  *  *   * root borg create borg-client@backup.domain.tld:/srv/client::{now:%Y-%m-%d} /path/to/save
```

# Commandes utiles

Sur le serveur de sauvegarde :

- Afficher la liste des jobs:
```
[root@backup ~]# borg list /srv/client
2020-08-15                           Tue, 2020-08-15 15:21:37 [5f0ffc068654b43b27d1b42d7ddc03f13901fbbfb8a34786627c75c80f98b022]
2020-08-22                           Tue, 2020-08-22 08:51:35 [07ac0ef9334d48d8f47d75075926e39631763820e50b270e58fc22c89ef50354]
```

- Obtenir le détail d'un job:
```
[root@backup ~]# borg info /srv/client/::2020-08-15
Utilization of maximum supported archive size: 1%
------------------------------------------------------------------------------
                       Original size      Compressed size    Deduplicated size
This archive:               41.39 TB             34.97 TB             53.63 MB
All archives:              109.73 TB             91.73 TB             33.81 TB

                       Unique chunks         Total chunks
Chunk index:                15152267             41411566
```
- Monter une sauvegarde:

```
[root@backup ~]# borg mount  /srv/client/::2020-08-15  /mnt/restore/
[root@backup ~]# borg umount /mnt/restore
```

- Contrôler l'intégrité d'une sauvegarde:

```
[root@backup ~]# borg check -v --progress /srv/client
```


# Restaurer les données

La sécurité apportée par la limitation des commandes en SSH ne permet de lancer une restauration directement depuis le client. Pour contourner le problème, il existe 2 solutions:
- modifier le fichier authorized_keys pour lever la limitation

```
[root@client ~]# borg mount borg-client@backup.domain.tld:/srv/client::2020-08-15
```

- monter de repository sur le serveur de backup et faire un rsync over ssh pour restaurer les données sur le client.

# Purger les données hors période de rétention

Là encore la limitation SSH impose d'utiliser la commande [borg prune](https://borgbackup.readthedocs.io/en/stable/usage/prune.html) en crontab du coté serveur de sauvegarde

```
# Keep 7 end of day and 4 additional end of week archives.
# Do a dry-run without actually deleting anything.
05 05  *  *   * root borg prune --keep-daily=7 --keep-weekly=4 /srv/client
```
> Dernier point ...pensez à décaler les jobs de sauvegardes et de purges entre les differents serveurs pour éviter un bottleneck au niveau réseau ou I/O disque

# Sources / docs...

- [site officiel](https://www.borgbackup.org)
- [documentation](https://borgbackup.readthedocs.io/en/stable/)
- https://blog.karolak.fr/post/2017-05-05-monter-un-serveur-de-sauvegarde-avec-borgbackup/
- https://connect.ed-diamond.com/Linux-Pratique/LP-098/Ne-procrastinez-plus-vos-sauvegardes-grace-a-Borg3
- https://sebsauvage.net/wiki/doku.php?id=borgbackup
