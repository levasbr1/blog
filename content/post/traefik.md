---
title: "Pris dans le Traefik" # Title of the blog post.
date: 2020-08-01T17:45:33+02:00 # Date of post creation.
description: "mise en oeuvre traefik" # Description used for search engine.
featured: true # Sets if post is a featured post, making appear on the home page side bar.
draft: false # Sets whether to render this page. Draft of true will not be rendered.
toc: false # Controls if a table of contents should be generated for first-level links automatically.
# menu: main
featureImage: "/images/traefik.logo.png" # Sets featured image on blog post.
thumbnail: "/images/traefik.logo.png" # Sets thumbnail image appearing inside card on homepage.
shareImage: "/images/path/share.png" # Designate a separate image for social media sharing.
codeMaxLines: 30 # Override global value for how many lines within a code block before auto-collapsing.
codeLineNumbers: false # Override global value for showing of line numbers within code block.
figurePositionShow: true # Override global value for showing the figure label.
categories:
  - DevOps
tags:
  - docker
  - traefik
series: ["Guide Traefik"]
---

Un premier post pour aborder l'incourtournable Traefik en reverse proxy http/tcp pour Docker.
<!--more-->

# Installation

On passe l'installation de Docker CE et Docker-Compose qui ne présente pas de [problème particulier](https://docs.docker.com/engine/install/).
L'ensemble des fichiers de configurations sera stocké dans l'arborescence suivante:


```bash
/opt
├── traefik
│   ├── config
│   │   ├── acme.json
│   │   ├── dynamic-conf
│   │   │   ├── dashboard.toml
│   │   │   ├── tlsredir.toml
│   │   │   ├── tls.toml
│   │   └── traefik.toml
│   └── docker-compose.yml
```

On commence par un docker-compose simple utilisant un bridge avec un subnet dédié:

```
 sudo docker network create --driver=bridge --subnet=172.19.0.0/24 traefik-lan
```

```yaml
version: '3'

services:
  reverse-proxy:
    image: traefik:v2.2
    restart: unless-stopped
    ports:
      - "80:80"
      - "443:443"
    expose:
      - "8080"
    networks:
      - traefik-lan
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock:ro
      - /opt/traefik/config:/etc/traefik:ro
      - /opt/traefik/config/acme.json:/acme.json
networks:
  traefik-lan:
    external: true
```

on compléte avec un fichier `traefik.toml` utilisant le mode dynamique (`providers.file`) qui permet de modifier la configuration sans relancer le conteneur.
L'opération est rendue possible grace au volume en bind mount `/opt/traefik/config:/etc/traefik`

On retrouve également la déclaration:

- des entryPoints en http / https
- la configuration Let's Encrypt pour un challenge TLS
- du formatage des logs.


```toml
[api]
  dashboard = true

[providers]
  [providers.docker]
    exposedByDefault = false

  [providers.file]
    directory = "/etc/traefik/dynamic-conf"
    watch = true

[entryPoints]
  [entryPoints.websecure]
    address = ":443"

  [entryPoints.webinsecure]
    address = ":80"


[certificatesResolvers.letsencrypt.acme]
  email = "my-email@domain.tld"
  storage = "acme.json"
  [certificatesResolvers.letsencrypt.acme.tlsChallenge]


[accessLog]
  format = "json"
  [accessLog.fields]
    defaultMode = "drop"
    [accessLog.fields.names]
      "ClientAddr" = "keep"
      "RequestAddr" = "keep"
      "RequestMethod" = "keep"
      "RequestPath" = "keep"
      "DownstreamStatus" = "keep"
```

On configure l'accés au dashboard en [mode secure](https://docs.traefik.io/v2.0/operations/dashboard/#secure-mode) via la configuration dynamique (`/opt/traefik/config/dynamic-conf/dashboard.toml`):

Au passage, on ajoute quelques restrictions:

- une whitelist (ça s'appelle encore comme ça chez Traefik) sur mon subnet local
- une http basic auth (à générer avec un `htpasswd -nb  user password`)

```toml
[http.routers.api]
  rule = "Host(`traefik.domain.tld`)"
  entrypoints = ["webinsecure"]
  service = "api@internal"
  middlewares = ["tlsredir@file"]

[http.routers.api-secure]
  rule = "Host(`traefik.domain.tld`)"
  entrypoints = ["websecure"]
  service = "api@internal"
  middlewares = ["secured"]
  [http.routers.api-secure.tls]
    certResolver = "letsencrypt"

[http.middlewares]
  [http.middlewares.secured.chain]
    middlewares = ["known-ips", "auth-users"]

[http.middlewares.auth-users.basicAuth]
  users = ["admin:$apr1$ZzA4BECk$dfdfsdfstCZN6.XH0lX1"]

[http.middlewares.known-ips.ipWhiteList]
  sourceRange = ["192.168.0.O/24"]
```

On force également l'accès en https (`/opt/traefik/config/dynamic-conf/tlsredir.toml`):

```toml
[http.middlewares]
  [http.middlewares.tlsredir.redirectScheme]
    scheme = "https"
    permanent = true
```

et pour finir, on affine la configuration TLS pour tenter le rang A+ sur [SSL Labs](https://www.ssllabs.com/ssltest/) (/opt/traefik/config/dynamic-conf/tls.toml)

```toml
[tls]
  [tls.options]
    [tls.options.default]
      minVersion = "VersionTLS12"
      sniStrict = true
      cipherSuites = [
        "TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384",
        "TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305",
      ]
      curvePreferences = ["CurveP521","CurveP384"]
```

La configuration de Traefik est maintenant terminée, il est temps de démarrer quelques services ....pour cela, il suffit d'ajouter un bloc de labels dans leurs docker-compose:

- la ligne `- "traefik.docker.network=traefik-lan"` est nécéssaire si vous avez plusieurs networks déclarés dans votre docker compose

```yaml
labels:
  - "traefik.enable=true"
  - "traefik.docker.network=traefik-lan"
  - "traefik.http.routers.<my-app>.rule=Host(`my-app.domain.tld`)"
  - "traefik.http.routers.<my-app>notls.entrypoints=webinsecure"
  - "traefik.http.routers.<my-app>enotls.middlewares=tlsredir@file"
  - "traefik.http.routers.<my-app>.rule=Host(`my-app.domain.tld`)"
  - "traefik.http.routers.<my-app>.entrypoints=websecure"
  - "traefik.http.routers.<my-app>e.tls=true"
  - "traefik.http.routers.<my-app>.tls.certresolver=letsencrypt"
```

# Remerciements

Un grand merci à Hugo (pas le CMS!) et aux blogs:
- [La Grotte du Barbu (FR)](https://www.grottedubarbu.fr)
- [Teddy FERDINAND (FR)](https://tferdinand.net)

# Sources / Docs...

- [la doc officielle (EN)](https://docs.traefik.io)
