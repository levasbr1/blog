---
title: "Web Analytics" # Title of the blog post.
date: 2020-09-03T11:59:26+02:00 # Date of post creation.
description: "Installation Fathom" # Description used for search engine.
featured: true # Sets if post is a featured post, making appear on the home page side bar.
draft: false # Sets whether to render this page. Draft of true will not be rendered.
toc: false # Controls if a table of contents should be generated for first-level links automatically.
# menu: main
featureImage: "/images/fathom.png" # Sets featured image on blog post.
thumbnail: "/images/fathom.png" # Sets thumbnail image appearing inside card on homepage.
shareImage: "/images/path/share.png" # Designate a separate image for social media sharing.
codeMaxLines: 10 # Override global value for how many lines within a code block before auto-collapsing.
codeLineNumbers: false # Override global value for showing of line numbers within code block.
figurePositionShow: true # Override global value for showing the figure label.
categories:
  - web
tags:
  - docker
  - traefik
---

Fathom est une alternative light à [Matomo](https://fr.matomo.org) pour connaitre l'audience d'un site web. En plus, il a le bon goût de tourner sous docker avec une base SQLite...

<!--more-->

# Installation

Cette installation repose sur l'infra [déployée précédemment]({{< ref "traefik.md" >}}) avec un exemple d'intégration dans [Hugo](https://gohugo.io)

Comme d'habitude, on part du [docker-compose.yml](https://github.com/usefathom/fathom/blob/master/docker-compose.yml) fourni par la projet et on supprime la partie MySQL pas forcément nécéssaire pour des petits sites. Au passage, j'ajoute un volume nommé pour persister la base SQLlite.

```yaml
version: '3'
services:
  fathom:
    image: usefathom/fathom:latest
    container_name: fathom
    expose:
      - "8080"
    environment:
      - 'FATHOM_SERVER_ADDR=:8080'
      - 'FATHOM_GZIP=true'
      - 'FATHOM_DEBUG=false'
      - 'FATHOM_DATABASE_DRIVER=sqlite3'
      - 'FATHOM_DATABASE_NAME=fathom.db'
      - 'FATHOM_SECRET=secret-password'
    volumes:
      - app:/app
    networks:
      - traefik-lan
    labels:
      - "traefik.enable=true"
      - "traefik.docker.network=traefik-lan"
      - "traefik.http.routers.fathomnotls.rule=Host(`webstat.domain.tld`)"
      - "traefik.http.routers.fathomnotls.entrypoints=webinsecure"
      - "traefik.http.routers.fathomnotls.middlewares=tlsredir@file"
      - "traefik.http.routers.fathom.rule=Host(`webstat.domain.tld`)"
      - "traefik.http.routers.fathom.entrypoints=websecure"
      - "traefik.http.routers.fathom.tls=true"
      - "traefik.http.routers.fathom.tls.certresolver=letsencrypt"

networks:
  traefik-lan:
    external: true

volumes:
  app:
```

# utilisation

Après le traditionnel `"docker-compose up -d"`, l'application est directement accessible sans authentification et vous propose de générer le `javascript` à intégrer à votre site.
Pour activer l'authentification et permettre d'intégrer plusieurs sites, il faut générer un identifiant:

```bash
docker exec <container_id> ./fathom user add --email="identifiant@domain.tld" --password="my-secret"
```

# Intégration dans Hugo

Rien de plus simple, il suffit de se rendre sur l'interface de votre Fathom et d'ajouter un nouveau site. L'application propose alors directement le javascript :

![screenshot fathom](/images/fathom-js.png)

Reste à l'intégrer dans Hugo :

```html
cat themes/<your-theme>/layouts/partials/fatham.html

<!-- Fathom - simple website analytics - https://github.com/usefathom/fathom -->
<script>
(function(f, a, t, h, o, m){
	a[h]=a[h]||function(){
		(a[h].q=a[h].q||[]).push(arguments)
	};
	o=f.createElement('script'),
	m=f.getElementsByTagName('script')[0];
	o.async=1; o.src=t; o.id='fathom-script';
	m.parentNode.insertBefore(o,m)
})(document, window, '//webstat.domain.tld/tracker.js', 'fathom');
fathom('set', 'siteId', 'OBUQO');
fathom('trackPageview');
</script>
<!-- / Fathom -->
```  
et de l'ajouter dans l'entête:

```html
cat themes/<your-theme>/layouts/partials/head.html
...
  {{ partial "fathom.html" . }}
</head>
```

Resultat final:

![screenshot fathom final](/images/fathom-stat.png)


# Sources / docs...  

- https://fredix.xyz/2019/06/stats-web-avec-fathom/
- https://github.com/usefathom/fathom
