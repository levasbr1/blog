---
title: "L'affaire est dans le sac" # Title of the blog post.
date: 2020-08-18T02:12:58+02:00 # Date of post creation.
description: "Installation d'une instance Wallabag" # Description used for search engine.
featured: true # Sets if post is a featured post, making appear on the home page side bar.
draft: false # Sets whether to render this page. Draft of true will not be rendered.
toc: false # Controls if a table of contents should be generated for first-level links automatically.
# menu: main
featureImage: "/images/wallabag-220.png" # Sets featured image on blog post.
thumbnail: "/images/wallabag-220.png" # Sets thumbnail image appearing inside card on homepage.
shareImage: "/images/path/share.png" # Designate a separate image for social media sharing.
codeMaxLines: 20 # Override global value for how many lines within a code block before auto-collapsing.
codeLineNumbers: false # Override global value for showing of line numbers within code block.
figurePositionShow: true # Override global value for showing the figure label.
categories:
  - Self-hosting
tags:
  - docker
  - traefik
---

J'utilise avec bonheur Framabag depuis de nombreuses années mais avec la fermeture prochaine du service, c'est l'occasion d'installer ma propre instance Wallabag.

<!--more-->

Wallabag est une solution libre de lecture de page web en différée à l'image de Pocket. Je l'utilise principalement pour faire de la veille techno et pour archiver les articles les plus intéressants. Vous pouvez souscrire à [l'offre SAS](https://www.wallabag.it/fr) ou installer votre propre instance. Il existe également une application Android sur [Google Play](http://play.google.com/store/apps/details?id=fr.gaulupeau.apps.InThePoche) ou [F-Droid](https://f-droid.org/en/packages/fr.gaulupeau.apps.InThePoche/)

# Installation  

Pour cette install, je profite de mon infra Docker / Traefik, ça tombe bien Wallabag propose directement un [docker-compose](https://github.com/wallabag/docker). Comme d'habitude, je procéde à quelques modifications par rapport au fichier d'origine.

> Attention, cet exemple de déploiement est dépendant de la configuration de Traefik [abordée ici]({{< ref "traefik.md" >}})

### Pour une utilisation avec un backend MariaDB :

- comme le port `80` est préempté par le conteneur Traefik, j'utilise la directive `expose` au lieu de `ports`
- je remplace les `bind mount` par des `named volumes`
- j'utilise un réseau dédié pour la connexion entre le conteneur applicatif, la database et Redis ainsi que celui reservé à Traefik


```yaml
version: '3'
services:
  wallabag:
    image: wallabag/wallabag:latest
    container_name: wallabag_app
    environment:
      - MYSQL_ROOT_PASSWORD=wallaroot
      - SYMFONY__ENV__DATABASE_DRIVER=pdo_mysql
      - SYMFONY__ENV__DATABASE_HOST=db
      - SYMFONY__ENV__DATABASE_PORT=3306
      - SYMFONY__ENV__DATABASE_NAME=wallabag
      - SYMFONY__ENV__DATABASE_USER=wallabag
      - SYMFONY__ENV__DATABASE_PASSWORD=my-secure-password
      - SYMFONY__ENV__DATABASE_CHARSET=utf8mb4
      - SYMFONY__ENV__MAILER_HOST=127.0.0.1
      - SYMFONY__ENV__MAILER_USER=~
      - SYMFONY__ENV__MAILER_PASSWORD=~
      - SYMFONY__ENV__FROM_EMAIL=wb-noreply@domain.tld
      - SYMFONY__ENV__DOMAIN_NAME=https://wb.domain.tld
    labels:
      - "traefik.enable=true"
      - "traefik.docker.network=traefik-lan"
      - "traefik.http.routers.wallabagnotls.rule=Host(`wb.domain.tld`)"
      - "traefik.http.routers.wallabagnotls.entrypoints=webinsecure"
      - "traefik.http.routers.wallabagnotls.middlewares=tlsredir@file"
      - "traefik.http.routers.wallabag.rule=Host(`wb.domain.tld`)"
      - "traefik.http.routers.wallabag.entrypoints=websecure"
      - "traefik.http.routers.wallabag.tls=true"
    expose:
      - "80"
    volumes:
      - app:/var/www/wallabag/web/assets/images
    restart: unless-stopped
    depends_on:
      - db
    networks:
      - lan
      - traefik-lan
```

on ajoute la database et le cache Redis

```yaml
db:
  image: mariadb
  container_name: wallabag_db
  environment:
    - MYSQL_ROOT_PASSWORD=my-secure-password
  volumes:
    - db:/var/lib/mysql
  restart: unless-stopped
  networks:
    - lan

redis:
  image: redis:alpine
  container_name: wallabag_redis
  networks:
    - lan
  restart: unless-stopped
```

Pour fonctionner, il faut déclarer les réseaux et les volumes dans le `docker-compose`:

```yaml
networks:
  lan:
    ipam:
      driver: default
      config:
        - subnet: "172.19.0.0/29"
  traefik-lan:
    external: true

volumes:
 db:
 app:
```

cette syntaxe permet d'obtenir:

```bash
[root@server wallabag]# docker network ls
NETWORK ID          NAME                DRIVER              SCOPE
ddb965f9d069        bridge              bridge              local
12df137c23d1        traefik-lan         bridge              local
76a52909db1d        wallabag_lan        bridge              local

[root@server wallabag]# docker volume ls
DRIVER              VOLUME NAME
local               wallabag_app
local               wallabag_db
```

# utilisation

Le docker-compoe est termniné, il suffit de faire un `docker-compose up -d` pour lancer votre instance Wallabag.
Cerise sur le gateau, vous pouvez exporter votre ancien wallabag au format JSON et le ré-impoter dans votre nouvelle instance.

![screenshot wallabag](/images/ss-wb.png)

Vous pouvez également ajouter un bookmark directement dans votre barre de favoris pour faciliter l'utilisation au quotidien ( menu "Aide" )

# Sources, docs...

- [la doc officielle (EN)](https://doc.wallabag.org/en/)
- Pour importer dans une liseuse Kobo:
  - https://chabotsi.fr/blog/wallabag-sur-votre-kobo-en-un-clic.html
  - https://gitlab.com/anarcat/wallabako
