---
title: "Spotify like" # Title of the blog post.
date: 2020-08-03T18:34:36+02:00 # Date of post creation.
description: "Installation Funkwhale" # Description used for search engine.
featured: true # Sets if post is a featured post, making appear on the home page side bar.
draft: false # Sets whether to render this page. Draft of true will not be rendered.
toc: false # Controls if a table of contents should be generated for first-level links automatically.
# menu: main
featureImage: "/images/funkwhale.png" # Sets featured image on blog post.
thumbnail: "/images/funkwhale.png" # Sets thumbnail image appearing inside card on homepage.
shareImage: "/images/path/share.png" # Designate a separate image for social media sharing.
codeMaxLines: 30 # Override global value for how many lines within a code block before auto-collapsing.
codeLineNumbers: false # Override global value for showing of line numbers within code block.
figurePositionShow: true # Override global value for showing the figure label.
categories:
  - Self-hosting
tags:
  - docker
  - traefik
---

Je cherchais depuis un moment une alternative libre à Spotify. J'ai finalement opté pour [Funkwhale](https://funkwhale.audio)...
<!--more-->

# Installation
Dans le prolongement du post sur [Traefik]({{< ref "traefik.md" >}}), je vous propose aujourd'hui de déployer une instance Funkwhale avec Docker.
Je pars du `docker-compose.yml` proposé dans la doc officielle et j'apporte quelques modifications:

- utilisation de `named volumes` au lieu de `bind mount`
- directive de `expose` au lieu de `ports` (inutile d'exposer un port sur la machine hôte)
- utilisation du réseau dédié à Traefik

> En l'absence de directive `container_name`, docker-compose va nommer les conteneurs en `<basedir>_<service-name>_<inc>`,  dans cette exemple ça donne: `funkwhale_funkwale_1`

```yaml
version: "3"
services:
  funkwhale:
    container_name: funkwhale
    restart: unless-stopped
    image: funkwhale/all-in-one:latest
    env_file: .env
    networks:
      - traefik-lan
    volumes:
      - app:/data
    expose:
      - "80"
    labels:
      - "traefik.enable=true"
      - "traefik.docker.network=traefik-lan"
      - "traefik.http.routers.funkwhalenotls.rule=Host(`music.domain.tld`)"
      - "traefik.http.routers.funkwhalenotls.entrypoints=webinsecure"
      - "traefik.http.routers.funkwhalenotls.middlewares=tlsredir@file"
      - "traefik.http.routers.funkwhale.rule=Host(`music.domain.tld`)"
      - "traefik.http.routers.funkwhale.entrypoints=websecure"
      - "traefik.http.routers.funkwhale.tls=true"
      - "traefik.http.routers.funkwhale.tls.certresolver=letsencrypt"

networks:
  traefik-lan:
    external: true

volumes:
 app:
```
> Cette configuration ne permet pas d'importer de la music depuis un répertoire local de la machine hôte -> voir doc si cette posibilité vous intéresse.

Il ne reste plus qu'a personnaliser le fichier d'environnement avec votre FQDN:

```bash
touch .env
chmod 600 .env
cat > .env << EOF
# Replace 'your.funkwhale.example' with your actual domain
FUNKWHALE_HOSTNAME=your.funkwhale.example
# Protocol may also be: http
FUNKWHALE_PROTOCOL=https
# This limits the upload size
NGINX_MAX_BODY_SIZE=100M
# Bind to localhost
FUNKWHALE_API_IP=127.0.0.1
# Container port you want to expose on the host
FUNKWHALE_API_PORT=5000
# Generate and store a secure secret key for your instance
DJANGO_SECRET_KEY=$(openssl rand -hex 45)
# Remove this if you expose the container directly on ports 80/443
NESTED_PROXY=1
EOF
```







# Sources, docs...
- [serveur 410 (FR)](https://serveur410.com/comment-se-barrer-de-spotify/)
- [documentation officielle (EN)](https://docs.funkwhale.audio)
