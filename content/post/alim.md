---
title: "Power supply..." # Title of the blog post.
date: 2020-08-25T15:59:46+02:00 # Date of post creation.
description: "alim pour chargeur DIY" # Description used for search engine.
featured: true # Sets if post is a featured post, making appear on the home page side bar.
draft: true # Sets whether to render this page. Draft of true will not be rendered.
toc: false # Controls if a table of contents should be generated for first-level links automatically.
# menu: main
featureImage: "/images/thunder.png" # Sets featured image on blog post.
thumbnail: "/images/thunder.png" # Sets thumbnail image appearing inside card on homepage.
shareImage: "/images/path/share.png" # Designate a separate image for social media sharing.
codeMaxLines: 10 # Override global value for how many lines within a code block before auto-collapsing.
codeLineNumbers: false # Override global value for showing of line numbers within code block.
figurePositionShow: true # Override global value for showing the figure label.
categories:
  - Modelisme
tags:
  - LiPo

---

Les alimentations pour chargeur de LiPo sont souvent hors de prix...
<!--more-->


# Sources / docs...

- [La bible sur RCGROUPS](https://www.rcgroups.com/forums/showthread.php?1292514-How-to-convert-Server-Power-Supplies)
